const cluster = require('cluster');
const express = require('express');
const app = express();

if (cluster.isMaster) {
    // Cause index.js to be executed again in the slave mode
    cluster.fork();
    cluster.fork();
    cluster.fork();
    cluster.fork();
} else {
    // This is a slave
    function doHardWork(duration) {
        const start = Date.now();
        while (Date.now() - start < duration) {}
    }

    app.get('/longwork/', (req, res) => {
        const { duration } = req.query;
        doHardWork(duration);
        res.send('I finished a long work');
    });

    app.get('/shortwork/', (req, res) => {
        res.send('I finsihed a short work');
    });

    app.listen(8089);
}